document.addEventListener("DOMContentLoaded", (event) => {
  // Get all the wrapper elements
  let wrappers = document.querySelectorAll(".cap-image_wrapper");

  // Add a flag to indicate when the mouse button or touch is down
  let isDown = false;

  // Define a function to handle mouse and touch move events
  function handleMove(event) {
    // Use either the mouse or touch position
    let clientX = event.clientX || event.touches[0].clientX;
    let clientY = event.clientY || event.touches[0].clientY;

    // Get the element's bounding rectangle
    let rect = this.getBoundingClientRect();

    // Calculate position relative to the element
    let x = clientX - rect.left;
    let y = clientY - rect.top;

    // Calculate the position in percentage
    let xPercent = Math.round((100 * x) / rect.width);
    let yPercent = Math.round((100 * y) / rect.height);

    // Get the image and the text
    let image = this.querySelector(".hover-img");
    let text = this.querySelector(".hover-div_text");

    // Use GSAP to animate the clip path and the text position
    gsap.to(this, {
      "--x": `${xPercent}%`,
      "--y": `${yPercent}%`,
      "--circleSize": `${isDown ? 100 : 15}%`,
      "--textX": `${x}px`,
      "--textY": `${y}px`,
      duration: 0.5,
      ease: "power2.out"
    });
  }

  // Define a function to handle mouse and touch start events
  function handleDown(event) {
    isDown = true;
    let text = this.querySelector(".hover-div_text");

    // Use GSAP to animate the clip path and the text opacity
    gsap.to(this, {
      "--circleSize": "120%",
      duration: 0.5,
      ease: "power2.out"
    });
    gsap.to(text, {
      opacity: "0",
      duration: 0.5,
      ease: "power2.out"
    });
  }

  // Define a function to handle mouse and touch end events
  function handleUp(event) {
    isDown = false;
    handleMove.call(this, event);
    let text = this.querySelector(".hover-div_text");
    gsap.to(text, {
      opacity: "1",
      duration: 0.5,
      ease: "power2.out"
    });
  }

  // Define a function to handle mouse out event
  function handleMouseOut(event) {
    let text = this.querySelector(".hover-div_text");

    // Get the element's bounding rectangle
    let rect = this.getBoundingClientRect();

    // Use either the mouse or touch position
    let clientX = event.clientX || event.touches?.[0].clientX;
    let clientY = event.clientY || event.touches?.[0].clientY;

    // Calculate position relative to the element
    let x = clientX - rect.left;
    let y = clientY - rect.top;

    // Calculate the position in percentage
    let xPercent = Math.round((100 * x) / rect.width);
    let yPercent = Math.round((100 * y) / rect.height);

    gsap.to(this, {
      "--x": `${xPercent}%`,
      "--y": `${yPercent}%`,
      "--circleSize": "15%",
      "--textX": `${x}px`,
      "--textY": `${y}px`,
      duration: 0.5,
      ease: "power2.out"
    });
    gsap.to(text, {
      opacity: "1",
      duration: 0.5,
      ease: "power2.out"
    });
  }

  // Apply the event handlers to all wrapper elements
  wrappers.forEach(function (wrapper) {
    // Mouse events
    wrapper.addEventListener("mousemove", handleMove);
    wrapper.addEventListener("mousedown", handleDown);
    wrapper.addEventListener("mouseup", handleUp);
    wrapper.addEventListener("mouseout", handleMouseOut);

    // Touch events
    wrapper.addEventListener("touchmove", handleMove);
    wrapper.addEventListener("touchstart", handleDown);
    wrapper.addEventListener("touchend", handleUp);
    wrapper.addEventListener("touchcancel", handleMouseOut);
  });
});
